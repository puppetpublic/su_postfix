# Defines a Postfix map.  This handles installing the source file and running
# postmap or postalias as needed.  Example for a map file:
#
#     su_postfix::map { '/etc/postfix/access/smtp':
#       source => 'puppet:///modules/s_emailrouter/etc/postfix/access/smtp',
#     }
#
# Example for an aliases file, which has to use a different command:
#
#     su_postfix::map { '/etc/postfix/aliases':
#       command => 'postalias',
#       source  => 'puppet:///modules/s_nagios/etc/postfix/aliases',
#     }
#
# Either source or content can be used with the map.
#
# Currently does not correctly support any type other than btree or hash.
# Adding the other hash types wouldn't be hard, but would require dynamically
# determining the extension and (for dbm and sdbm) handling the creation of
# two files (*.pag and *.dir) instead of one.  So we'll wait until someone
# actually needs that.

define su_postfix::map(
  Enum['absent', 'present'] $ensure  = 'present',
                            $command = 'postmap',
  Enum['hash', 'btree']     $type    = 'hash',
                            $content = undef,
                            $source  = undef)
{
  file { $name:
    ensure  => $ensure,
    content => $content,
    source  => $source,
    notify  => Exec["${command} ${type}:${name}"],
    require => Package['postfix'],
  }

  # If ensure is absent, remove the detritus of our hash.
  if ($ensure == 'absent') {
    file { "${name}.db": ensure => $ensure }
  } else {
    # We have to define two commands here.  The first uses creates and is
    # responsible for ensuring that the *.db file always exists (and is only
    # defined if the type is hash).  The second is called by notify and is
    # responsible for updating the hash when the source file changes.  We need
    # both because a command with a creates stanza won't run even if notified
    # if that file already exists.
    exec { "${command} ${type}:${name} initial":
      path    => '/bin:/usr/sbin:/usr/bin',
      command => "${command} ${type}:${name}",
      creates => "${name}.db",
      require => [
        File[$name],
        File['/etc/postfix/main.cf'],
        Package['postfix']
      ],
    }
    exec { "${command} ${type}:${name}":
      refreshonly => true,
      path        => '/bin:/usr/sbin:/usr/bin',
      command     => "${command} ${type}:${name}",
      require     => [ File['/etc/postfix/main.cf'], Package['postfix'] ],
    }
  }
}
