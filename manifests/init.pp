# Standard Postfix class for most systems.

# Standard Postfix class for outgoing servers.  There is no daemon listening
# on SMTP port.  The outgoing mail will be masqueraded except root mail.
#
# The files managed by this class:
#
# * /etc/postfix/main.cf
# * /etc/postfix/master.cf
# * /etc/postfix/recipients
# * /etc/postfix/transport
# * /etc/postfix/senders
#
# Set $is_server parameter to true if node will act as an SMTP server.
#
# $enable_postfix_compat2: if true, will add the line
#
#    compatibility_level = 2
#
# to /etc/postfix/main.cf. For more information, see
# http://www.postfix.org/COMPATIBILITY_README.html#turnoff
#
# $sender_rootmail_only: set this to true if you are only ever going to have a
# SINGLE line in /etc/postfix/senders corresponding to root@stanford.edu. Otherwise,
# leave it at its default of false.
#
# $is_server: set to true when the Puppet client is both an incoming and
#   outgoing mail server. Standard Postfix server configuration that allows
#   both incoming and outgoing mail. Override things selectively so that we
#   accept mail from the network and deliver mail locally.

class su_postfix (
  Boolean $enable_postfix_compat2 = true,
  Boolean $manage_postfix_main    = true,
  Boolean $manage_postfix_master  = true,
  Boolean $sender_rootmail_only   = false,
  #
  Boolean $is_off_campus          = false,
  Boolean $is_server              = false,
) {
  package { 'postfix': ensure => present }


  # Create the postfix service:
  include su_postfix::service

  $mastercfsuffix = $::osfamily

  # Basic configuration files.
  file {  '/etc/filter-syslog/postfix':
    source  => 'puppet:///modules/su_postfix/etc/filter-syslog/postfix';
  }

  # Manage main.cf and master.cf
  class { 'su_postfix::config::main_cf':
    is_managed             => $manage_postfix_main,
    is_server              => $is_server,
    is_off_campus          => $is_off_campus,
    enable_postfix_compat2 => $enable_postfix_compat2,
  }

  class { 'su_postfix::config::master_cf':
    is_managed    => $manage_postfix_master,
    is_server     => $is_server,
    is_off_campus => $is_off_campus,
  }

  # Manage some Postfix "map" files.
  su_postfix::map {
    '/etc/postfix/recipients':
      ensure  => present;
    '/etc/postfix/transport':
      source  => 'puppet:///modules/su_postfix/etc/postfix/transport';
  }

  # Manage /etc/postfix/senders.
  class { 'su_postfix::config::senders':
    sender_rootmail_only => $sender_rootmail_only,
    is_off_campus        => $is_off_campus,
  }

  # We used to disable and stop sendmail here on Red Hat, but this doesn't
  # work if sendmail is not installed.  Since we'd rather not have sendmail
  # installed, just assume that people are doing this manually.
}
