class su_postfix::service (
){
  # The core service.  The init script appears to support status on all the
  # operating systems we care about.  Define a custom reload action so that
  # we can just signal the service when we change configuration files
  # without stopping and restarting the daemons, since Postfix can re-read
  # all its configuration files with a reload.
  service { 'postfix':
    ensure    => running,
    enable    => true,
    hasstatus => true,
    restart   => '/usr/sbin/postfix reload',
    require   => [
      File['/etc/postfix/main.cf'],
      File['/etc/postfix/master.cf'],
      Package['postfix']
    ],
  }

}
