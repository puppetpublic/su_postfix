# modules/postfix/policyd.pp -- postfix-policyd engine model
#
# homepage: http://www.policyd.org/

class su_postfix::policyd {
    preseed_debconf { 'preseed-postfix-policyd':
        source => 'puppet:///modules/su_postfix/etc/apt/preseed-postfix-policyd',
        answer => 'postfix-policyd.*boolean.*false',
    }

    package { 'postfix-policyd':
        ensure  => installed,
        require => Preseed_debconf['preseed-postfix-policyd'],
    }

    exec { 'update-rc.d -f postfix-policyd remove':
        command     => '/usr/sbin/update-rc.d -f postfix-policyd remove',
        subscribe   => Package['postfix-policyd'],
        refreshonly => true,
    }

    file {
        '/etc/postfix-policyd.conf':
            ensure  => absent,
            require => Package['postfix-policyd'];
        '/etc/postfix-policyd':
            ensure  => directory,
            require => Package['postfix-policyd'];
        '/etc/postfix-policyd/letter':
            require => File['/etc/postfix-policyd'],
            source  => 'puppet:///modules/su_postfix/etc/postfix-policyd/letter';
    }
}
