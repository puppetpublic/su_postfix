# Create a Postfiix policyd file (need more documentation)
define su_postfix::policyd::config(
  Enum['absent', 'present'] $ensure = 'present',
) {
    $policy_name = $name

    $dbconfig = '/etc/postfix-policyd/policyd-db'
    $template = "/etc/postfix-policyd/${name}.conf.tmpl"
    $config   = "/etc/postfix-policyd/${name}.conf"
    $genconf  = "/usr/bin/generate-conf -c ${dbconfig} -t ${template} -n ${config}"

    exec { "restart policyd-${name}":
        command     => "/usr/sbin/invoke-rc.d policyd-${name} restart",
        refreshonly => true,
    }

    exec { "update-rc.d policyd-${name} defaults":
        command     => "/usr/sbin/update-rc.d policyd-${name} defaults",
        refreshonly => true,
    }

    exec { "generate policyd-${name} config":
        command     => $genconf,
        notify      => Exec["restart policyd-${name}"],
        subscribe   => File[$template],
        refreshonly => true,
    }

    case $ensure {
        present: {
            file {
                "/etc/init.d/policyd-${name}":
                    content => template('su_postfix/policyd.init.erb'),
                    notify  => Exec["update-rc.d policyd-${name} defaults"],
                    mode    => '0755';
                $template:
                    source  => "puppet:///modules/su_postfix/${template}",
                    require => [
                      File["/etc/init.d/policyd-${name}"],
                      File['/etc/postfix-policyd'],
                      File[$dbconfig]
                    ],
                    notify  => Exec["generate policyd-${name} config"];
            }

            service { "policyd-${name}":
                ensure     => running,
                hasrestart => true,
                enable     => true,
                require    => [ File["/etc/init.d/policyd-${name}"],
                                File[$template] ];
            }
        }
        absent: {
        }
        default: {
          crit('unknown case')
        }
    }
}
