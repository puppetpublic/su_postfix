# Our default Postfix configuration disables all local delivery and therefore
# doesn't use the aliases file.  Instead, any mail routing other than
# masquerading as stanford.edu needs to be handled by the recipients map.
# This creates a class that can be used to add and remove entries from the
# recipients map.
#
# Example of how to set a recipient:
#
#     su_postfix::recipient { 'root@stanford.edu':
#       ensure => 'alert-email@monitoring.stanford.edu'
#     }
#
# Example of how to ensure there is no recipient map:
#
#     su_postfix::recipient { 'luke': ensure => absent }
#
# Assumes that a command 'postmap hash:$file' has been defined that will
# rebuild the recipients map.

define su_postfix::recipient(
  $ensure,
  $file = '/etc/postfix/recipients'
) {
  $pattern = "'^${name}'"
  case $ensure {
    'absent': {
      exec { "rm-recipient-${name}":
        path    => '/bin:/usr/sbin:/usr/bin',
        command => "sed -i -e '/^${name}/d' ${file}",
        onlyif  => "grep ${pattern} ${file}",
        notify  => Exec["postmap hash:${file}"]
      }
    }
    default: {
      $line = "${name} ${ensure}"
      exec { "add-recipient-${name}":
        path    => '/bin:/usr/sbin:/usr/bin',
        command => "echo '${line}' >> ${file}",
        unless  => "grep ${pattern} ${file}",
        require => Package['postfix'],
        notify  => Exec["postmap hash:${file}"],
      }
      exec { "fix-recipient-${name}":
        path    => '/bin:/usr/sbin:/usr/bin',
        command => "sed -i -e 's/^${name}..*\$/${line}/' ${file}",
        unless  => "grep '^${line}\$' ${file}",
        require => Exec["add-recipient-${name}"],
        notify  => Exec["postmap hash:${file}"],
      }
    }
  }
}
