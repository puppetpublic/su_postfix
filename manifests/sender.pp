# Many systems send emails "From" root@system-name.  That causes problems when
# an email is bounced.  This type is used to have Postfix change the sender
# address of an email while it is in flight.  Both the From: header and the
# envelope sender are changed.
#
# Example of how to set a sender:
#
#     su_postfix::sender { 'root@stanford.edu':
#       ensure => 'noreply@stanford.edu'
#     }
#
# Example of how to ensure there is no sender map:
#
#     su_postfix::sender { 'luke': ensure => absent }
#
# Assumes that a command 'postmap hash:$file' has been defined that will
# rebuild the senders map.

define su_postfix::sender(
  $ensure,
  $file = '/etc/postfix/senders'
) {
  $line = "${name} ${ensure}"
  $file_line_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'present',
  }

  file_line { "add-sender-${name}":
    ensure  => $file_line_ensure,
    path    => $file,
    line    => $line,
    require => Package['postfix'],
    notify  => Exec["postmap hash:${file}"],
  }
}
