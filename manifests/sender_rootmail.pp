# Many systems send emails "From" root@system-name.  That causes problems when
# an email is bounced.  This type is used to have Postfix change the sender
# address of an email while it is in flight.  Both the From: header and the
# envelope sender are changed.
#
# This class is based on postfix::sender but is different in that it ONLY
# lets /etc/postfix/sender have the single line "root@stanford.edu root@FQDN".
# This avoids the problem that postfix::sender has where a hostname change can
# cause there to be TWO entries in /etc/postfix/sender for "root@stanford.edu".
#
# You cannot use this class and postfix::sender at the same time as they
# manage the same file.
#
# Only use this class when you want the single line in /etc/postfix/senders for
# rootmail.

class su_postfix::sender_rootmail(
  $ensure = 'present',
  $file = '/etc/postfix/senders'
) {

  file { $file:
    ensure  => $ensure,
    content => template('su_postfix/sender_rootmail.erb'),
    require => Package['postfix'],
    notify  => Exec["postmap hash:${file} sender_rootmail"],
  }

  exec { "postmap hash:${file} sender_rootmail":
    refreshonly => true,
    path        => '/bin:/usr/sbin:/usr/bin',
    command     => "postmap hash:${file}",
    require     => [ File[$file], Package['postfix'] ],
  }

}
