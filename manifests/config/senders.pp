# Manage the /etc/postfix/senders file.
class su_postfix::config::senders (
  Boolean $sender_rootmail_only = undef,
  Boolean $is_off_campus        = undef,
) {

  # Have mail from root@stanford.edu come from root at the local system.

  # If $sender_rootmail_only is true this means that we want the
  # su_postfix::sender_rootmail class to manage /etc/postfix/senders
  # file and so we cannot let su_postfix::map manage
  # /etc/postfix/senders (otherwise we would get a duplicate resource
  # error).

  if ($sender_rootmail_only) {
    include su_postfix::sender_rootmail
  } else {
    if ($is_off_campus) {
      # Off-campus outgoing email server only to stanford.edu addresses.
      su_postfix::map { '/etc/postfix/senders':
        ensure  => present,
        content => "root@stanford.edu root@${::fqdn_lc}\n",
      }
    } else {
      su_postfix::map { '/etc/postfix/senders':
        ensure  => present;
      }
    }
    su_postfix::sender { 'root@stanford.edu':
      ensure => "root@${::fqdn}";
    }
  }

}
