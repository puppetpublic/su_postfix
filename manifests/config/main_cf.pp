# Mange the /etc/postfix/main.cf file.
class su_postfix::config::main_cf (
  Boolean $is_managed             = undef,
  Boolean $is_server              = undef,
  Boolean $is_off_campus          = undef,
  Boolean $enable_postfix_compat2 = undef,
) {

  # An off-campus postfix client is not allowed to be a server.
  if ($is_off_campus and $is_server) {
    fail('an off-campus machine cannot be a server')
  }

  if ($is_managed) {
    if ($is_server) {
      # IS A SERVER:
      $content_filename = 'main-server.cf.erb'
    } elsif ($is_off_campus) {
      # IS OFF-CAMPUS:
      $content_filename = 'main.cf.off-campus.erb'
    } else {
      # EVERYTHING ELSE:
      $content_filename = 'main.cf.erb'
    }

    file { '/etc/postfix/main.cf':
      content => template("su_postfix/etc/postfix/${content_filename}"),
      notify  => Service['postfix'],
      require => Package['postfix'],
    }
  }

}
