# Manage the /etc/postfix/master.cf file.
class su_postfix::config::master_cf (
  Boolean $is_managed    = undef,
  Boolean $is_server     = undef,
  Boolean $is_off_campus = undef,
) {

  if ($is_managed) {
    $mastercfsuffix = $::osfamily

    if ($is_server) {
      # IS A SERVER:
      $source_filename = "master-server.cf.${mastercfsuffix}"
    } else {
      # EVERYTHING ELSE:
      $source_filename  = "master.cf.${mastercfsuffix}"
    }

    file { '/etc/postfix/master.cf':
      source  => "puppet:///modules/su_postfix/etc/postfix/${source_filename}",
      notify  => Service['postfix'],
      require => Package['postfix'];
    }
  }

}
