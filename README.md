A Postfix Puppet module extracted from the legacy base::postfix Puppet module.

IMPORTANT: The legacy base::postfix added iptables rules to allow incoming
STMP traffic for SMTP servers. The su_postfix module does _not_ do this. You must
open up those ports yourself separately. If you are still using
`base::iptables` you accomplish this like so:
```
base::iptables::rule { 'smtp':
  description => 'Allow incoming SMTP traffic from anywhere',
  protocol    => 'tcp',
  port        => [ 25, 465, 587 ],
}
```


